## Introdução
Javascript é hoje uma das linguagens mais escolhidas da atualidade para o desenvolvimento de Aplicações. É possível encontrá-lo em aplicações de todo tipo, mas sem dúvida possui maior predomínio nas aplicações web que rodam em navegadores. Há poucos anos, a linguagem era mal vista, tinha fama de ser uma bagunça, porém diversas inovações em seu ecossistema possibilitaram que a linguagem subisse de nível e se tornasse a linguagem que é hoje. 

## História

Javascript foi criado na segunda metade do ano de 1995. Na época a Netscape estava entrando no mercado de navegadores de internet com o Netscape Navigator e visando expandir seu market share, o mercado era dominado pelo Mosaic da NCSA (National Center for Supercomputing Applications).
A Netscape entendeu que para ter um diferencial de mercado bom o bastante para superar o Mosaic precisaria explorar alguma forma de trazer mais dinâmica aos sites – por meio de animações, interações, coisas pequenas. Além disso eles tinham em mente possibilizar o desenvolvimento de software facilitado para pessoas fora da área da computação, como designers de interfaces.
Para atender a essa demanda a Netscape alocou Brendan Eich na criação dessa linguagem que seria embarcada no seu Browser. Porém, o projeto tinha uma restrição de tempo muito grande, fazendo com que fosse cogitado utilizar alguma linguagem já existente, como Python. Mas, Brendan optou por desenvolver uma linguagem nova a partir do Scheme. Existia uma tensão para fechar um acordo com a Sun Microsystems que gostaria de oferecer aos seus clientes como uma  espécie de extensão do Java para os Browsers. Para garantir o acordo a Netscape adaptou a sintaxe da linguagem para algo mais parecido com Java.
Portanto, a linguagem foi criada parecida com Java na sintaxe, mas uma linguagem funcional, por debaixo dos panos.

## Características Técnicas

### Paradigmas Suportados

Ela é uma linguagem de scripting baseada em protótipos, multi-paradigma e dinâmica, suportando os estilos orientado a objetos, imperativo e funcional.

### Domínios Utilizados

Aplicações Desktop, Web, Mobile, Servidores Web, Robótica, Inteligência artificial, Data Science.

### Legibilidade/Simplicidade/Ortogonalidade

Legibilidade: Tinha muitos problemas de legibilidade na sua sintaxe antiga "vanilla". Hoje com a evolução da linguagem esse aspecto melhorou muito.

Simplicidade: A linguagem em geral tem código simples quando temos um software básico ou intermediário em sua complexidade, mas pode se tornar complicada, por conta da orientação a objetos orientada a protótipo.

Ortogonalidade: A linguagem possui poucos tipos primitivos, mas por conta da sua arquitetura onde toda função é um objeto de primeira classe. Sendo assim, é possivel retornar objetos, funções, dentre outras coisas, de qualquer função, o que acaba tornando muito complicado considerá-la uma linguagem ortogonal. Além disso, com a adição da nova sintaxe, agora é necessário saber duas sintaxes para poder trabalhar com a linguagem profissionalmente.

### Suporte a abstração

Não possui interfaces, nem classes abstratas, mas é possível herança simples. Na sintaxe antiga a herança ocorre através dos prototypes. Na sintaxe nova, segunda a mozilla, existe um mecanismo chamado mix-ins que provoca um comportamento parecido com o da classe abstrata ou herança múltipla.

### Verificação de Tipos

A linguagem possui tipagem fraca e sendo assim infere os tipos por conta própria. Existem mecanismos da linguagem para verificar tipos e para fazer conversão explicita para algum tipo primitivo. 

### Tratamento de Exceções

No javascript, durante a execução de um programa, podem ser geradas exceções. As exceções são jogadas no console ou podem ser capturadas pelo desenvolvedor dentro dos blocos try..catch e lá dentro, tratadas para evitar que a aplicação quebre.

### Suporte a programação concorrente

Possui modelo de concorrência baseado no Event loop (laço de eventos), responsável pela execução do código, coleta e processamento de eventos e execução de subtarefas enfileiradas.

Basicamente a linguagem trabalha com uma fila de execução de mensagens síncrona. Enquanto houverem eventos na fila eles serão executados um de cada vez.

```javascript
while (queue.waitForMessage()) {
  queue.processNextMessage();
}
```

O interessante é que essa estrutura faz com que o código javascript não bloqueie a execução, ele confia as mensagens na fila e segue com a execução do resto do código. Quando o evento é executado ele chama a função de callback que foi passada como parâmetro de execução.

### Suporte a XML e JSON

XML e JSON são atualmente os princiais padrões de integração de aplicações e ambos são suportados no Javascript nativamente nos browsers.

### Máquina Virtual

O javascript é uma linguagem que depende de uma máquina virtual, e existem várias, para ser executado.
No mozilla temos a primeira máquina criada, a SpiderMonkey, no Chrome temos a V8 - utilizada no NodeJs, Electron e DenoJs - e no Safari temos o JavaScriptCore. Todos eles funcionam de formas diferentes.
Eles não necessariamente compilam o código, no chrome a compilação é usada caso, após a interpretação (Ignition), ele detecte que parte do código (agora em byte-code) precise passar por uma otimização, então ele notifica ao compilador TurboFan que por sua vez, gera código altamente otimizado e substitui no byte-code original

### Análise Léxica

Na virtual machine do google (V8) o ignition parser identifica os tokens e cria árvores abstratas de sintaxe (AST’s)

### Compilação/Otimização

As AST’s são enviadas para o interpretador (Ignition) que faz otimização e interpretação do código, caso seja necessário mais otimização em algumas partes o código é enviado para o TurboFan que compila essas partes e gera novo
byte-code mais otimizado.

### Variáveis/Ciclo de Vida/Armazenamento em Memória

Javascript é uma linguagem em transição. Existem
atualmente duas sintaxes válidas e isso afeta os tipos de variáveis. É possível declarar variáveis com a palavra reservada var - da sintaxe antiga - que tem escopo de função, e entra numa regra peculiar desta linguagem que é o hoisting. O hoisting significa que as variáveis declaradas com var - antes da execução do código - são “declaradas” (alocam-se na memória como variáveis estáticas), ou seja, acima de todo o código que será executado, considerando o escopo da função. A partir do ecmascript es6 em 2015,
foram adicionadas as palavras chave let - variável - e const - constante - que funcionam como códigos em c, ou java, onde o escopo delas é o do seu bloco de execução e não entram na regra do hoisting (e sim, são alocadas na memória em tempo de
execução).

### Suporte para variáveis "static"

Variáveis ou funções estáticas possuem palavra chave específica e funcionam, como no java, desde o es6. Além disso é possível, desde sempre, definir atributo para funções e objetos, uma vez que as funções em javascript são cidadãos de primeira classe.
Convêm, então, explicar que isso significa que as funções são tratadas como qualquer outra variável, sendo possível, por exemplo passar funções como parâmetros ou retornar uma
função (callback).

### Variáveis de Pilha (Stack)

As funções são alocadas na pilha de chamadas e saem da pilha assim que são retornadas. Elas agem da forma first in last out, assim como em outras linguagens. Esse mecanismo permite a recursão.

### Tail Call

Até o presente momento a tail call não foi implementada no javascript, apesar da ecmascript já ter citado a intenção na especificação do es6 em https://dev.to/snird/recursion-optimization-in-js-where-is-it-ptc-tcoand-fud-4fka
Faça um exemplo. 2015. Apesar disso, o motor javascript, por trás do navegador safari, já implementou essa funcionalidade. Em suma, implementar o tail call eliminaria a desvantagem da recursividade contra a iteratividade. A linguagem lua já tem isso implementado há bastante
tempo e tem se mostrado promissora.

### Estruturas de Controle

Javascript, por sua história, tem sintaxe familiar aos desenvolvedores Java e C. Portanto, possui if..else,
switch, while, do..while, for, break, continue, além da estrutura de controle de erro try..catch. Javascript
possui estruturas de controle de fluxo assíncrono também como as Promisses, async..await, yield

### Estruturas de Seleção

seleção simples - if..else, if..else if… else - e seleção múltipla - switch

## Referências

https://github.com/getify/You-Dont-Know-JS/blob/2nd-ed/get-started/ch1.md#many-faces

https://auth0.com/blog/a-brief-history-of-javascript/

https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Classes

https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/About_JavaScript

https://www.devmedia.com.br/programacao-orientada-a-objetos-x-programacao-estruturada/31852

https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/EventLoop

https://www.w3schools.com/xml/xml_parser.asp

https://www.geeksforgeeks.org/what-happens-inside-javascript-engine/

https://blog.logrocket.com/how-javascript-works-optimizing-the-v8-compiler-for-efficiency/

https://v8.dev/blog/scanner

https://www.digitalocean.com/community/tutorials/understanding-variables-scope-hoisting-in-javascript

https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/classes/static

https://medium.com/@allansendagi/javascript-fundamentals-call-stack-and-memory-heap-401eb8713204

https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Guide/Declara%C3%A7%C3%B5es


